package com.gitlab.edufuga.wordshelf

import com.gitlab.edufuga.wordlines.core.{Status, WordStatusDate}
import com.gitlab.edufuga.wordlines.recordreader.{RecordReaderService, Word}
import com.gitlab.edufuga.wordlines.recordwriter.RecordWriterService
import com.gitlab.edufuga.wordlines.sentenceparser.SentenceParserService
import com.google.gson.GsonBuilder
import spark.Spark._
import spark._

import scala.jdk.CollectionConverters._

object App {
  def main(args: Array[String]): Unit = {
    val recordReaderService: RecordReaderService = new RecordReaderService()
    val recordWriterService: RecordWriterService = new RecordWriterService()
    val sentenceParserService: SentenceParserService = new SentenceParserService()

    get("/word/:language/:word", (request: Request, response: Response) => {
      val language = request.params(":language")
      val word = request.params(":word")

      response.`type`("application/json")

      recordReaderService.readRecord(language, word)
    })

    get("/words/:language", (request: Request, response: Response) => {
      val language = request.params(":language")

      response.`type`("application/json")

      val gsonBuilder = new GsonBuilder
      gsonBuilder.setPrettyPrinting()
      val gson = gsonBuilder.create

      val wordsRaw: List[String] = recordReaderService.getWords(language).asScala.toList
      val words: List[Word] = wordsRaw.sorted
        .map(w => new Word(w, request.scheme() + "://" + request.host() + "/word/" + language + "/" + w))

      gson.toJson(words.asJava)
    })

    get("/records/:language", (request: Request, response: Response) => {
      val language = request.params(":language")

      response.`type`("application/json")

      val gsonBuilder = new GsonBuilder
      gsonBuilder.setPrettyPrinting()
      val gson = gsonBuilder.create

      val records: List[WordStatusDate] = recordReaderService.getRecords(language).asScala.toList

      gson.toJson(records.sortBy(_.getWord).asJava)
    })

    get("/records/:language/:state", (request: Request, response: Response) => {
      val language = request.params(":language")
      val state = Status.valueOf(request.params(":state").toUpperCase)

      response.`type`("application/json")

      val gsonBuilder = new GsonBuilder
      gsonBuilder.setPrettyPrinting()
      val gson = gsonBuilder.create

      val records: List[WordStatusDate] = recordReaderService.getRecords(language).asScala.toList
      val recordsWithState: List[WordStatusDate] = records.filter(r => state == r.getStatus)
      gson.toJson(recordsWithState.sortBy(_.getWord).asJava)
    })


    get("/word/:language/:word/:state", (request: Request, response: Response) => {
      val language = request.params(":language")
      val word = request.params(":word")
      val state = request.params(":state").toUpperCase

      response.`type`("application/json")

      val writtenRecord = recordWriterService.writeRecord(language, word, state)

      val gsonBuilder = new GsonBuilder
      gsonBuilder.setPrettyPrinting()
      val gson = gsonBuilder.create

      gson.toJson(writtenRecord)
    })

    get("/sentence/:language/:sentence", (request: Request, response: Response) => {
      val language = request.params(":language")
      val sentence = request.params(":sentence")

      response.`type`("application/json")

      val parsedSentence = sentenceParserService.parseSentence(language, sentence)

      val gsonBuilder = new GsonBuilder
      gsonBuilder.setPrettyPrinting()
      val gson = gsonBuilder.create

      gson.toJson(parsedSentence)
    })
  }
}
